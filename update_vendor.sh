#!/bin/sh

set -e

VENDOR_GIT=git@github.com:sippy/rtpproxy.git

TMPDIR=`mktemp -d /tmp/rtpproxy.git.XXXXXX`

git clone ${VENDOR_GIT} ${TMPDIR}

VENDOR_REV=`git -C ${TMPDIR} log | grep ^commit | head -n1 | awk '{print $2}'`

cd ${TMPDIR}
./configure
cp src/config.h src/config_std.h
make distclean
cd -

if [ -e config.h ]
then
  rm config.h
fi
git rm -rf *.[ch] makeann extractaudio udp_storm Makefile Makefile.makeann
cp -Rp ${TMPDIR}/src/*.[ch] ${TMPDIR}/makeann \
  ${TMPDIR}/extractaudio ${TMPDIR}/udp_storm ./
cp ${TMPDIR}/mk/* ./
rm -rf ${TMPDIR}
if [ -e config.h ]
then
  rm config.h
fi
git add -A *.[ch] makeann extractaudio udp_storm Makefile Makefile.makeann

git commit -m "Push update from GitHub, branch master, version ${VENDOR_REV}" .
git tag github.${VENDOR_REV}
git push -u origin --all
git push -u origin --tags
